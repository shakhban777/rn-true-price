/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, {useEffect, useRef, useState} from 'react';
import {
  NativeSyntheticEvent,
  SafeAreaView,
  ScrollView,
  StatusBar,
  TextInput,
  TextInputChangeEventData,
  useColorScheme,
  View,
  Pressable,
} from 'react-native';

import {Input} from './Input';
import {Text} from './Text';
import {
  containsLetters,
  convertPrice,
  numberHasTwoOrMoreDots,
} from '../utils/helper';
import {styles} from './styles';

type Products = {
  price: string;
  weight: string;
  cost: string;
};

type BlockProps = {
  children: string | JSX.Element | JSX.Element[];
};

const Block: React.FC<BlockProps> = ({children}) => {
  return <View style={styles.row}>{children}</View>;
};

const productItem = {
  price: '',
  weight: '',
  cost: '0',
};

function App(): JSX.Element {
  const [products, setProducts] = useState<Products[]>([productItem]);
  const isDarkMode = useColorScheme() === 'dark';

  const ref_input = useRef<TextInput | null>(null);

  useEffect(() => {
    const updatedProducts = products.map(product => {
      if (product.price && product.weight) {
        const updatedCost = (+product.price / +product.weight) * 1000;
        const convertedPrice = convertPrice(updatedCost);

        return {
          ...product,
          cost: convertedPrice.toString(),
        };
      }

      return product;
    });

    setProducts(updatedProducts);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(products)]);

  const onChangeValue = (
    e: NativeSyntheticEvent<TextInputChangeEventData>,
    index: number,
    filedName: string,
  ) => {
    if (
      numberHasTwoOrMoreDots(e.nativeEvent.text) ||
      containsLetters(e.nativeEvent.text)
    ) {
      return;
    }

    const updatedProducts = products.map((product, i) => {
      if (index === i) {
        return {
          ...product,
          [filedName]: e.nativeEvent.text,
        };
      }

      return product;
    });
    setProducts(updatedProducts);
  };

  const onAddNewFields = async () => {
    const newProducts = [...products, productItem];
    console.log({newProducts});
    await setProducts(newProducts);
  };

  const onClearFields = () => {
    setProducts([productItem]);
  };

  return (
    <SafeAreaView style={styles.backgroundStyle}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={styles.backgroundStyle.backgroundColor}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.backgroundStyle}>
        {products.map((product, index) => (
          <View
            style={[
              styles.main,
              index === products.length - 1 && styles.lastChild,
            ]}
            key={index}>
            <Block>
              <Text>Цена</Text>
              <Input
                value={product.price}
                onChange={e => onChangeValue(e, index, 'price')}
                keyboardType="numeric"
                autoFocus={true}
                onSubmitEditing={() => ref_input.current?.focus()}
              />
            </Block>

            <Block>
              <Text>Вес, г</Text>
              <Input
                value={product.weight}
                onChange={e => onChangeValue(e, index, 'weight')}
                keyboardType="numeric"
                ref={ref_input}
                onSubmitEditing={() => ref_input.current?.blur()}
              />
            </Block>

            <Block>
              <Text>Стоимость за 1 кг</Text>
              <Input value={product.cost} editable={false} />
            </Block>
          </View>
        ))}
      </ScrollView>

      <View style={styles.footer}>
        <Pressable
          style={({pressed}) => [
            {
              backgroundColor: pressed ? '#FF5C2B' : '#FF771C',
            },
            styles.button,
          ]}
          onPress={onAddNewFields}>
          <Text>+</Text>
        </Pressable>
      </View>

      <View>
        <Pressable style={styles.clearButton} onPress={onClearFields}>
          <Text>Очистить</Text>
        </Pressable>
      </View>
    </SafeAreaView>
  );
}

export default App;
