import React from 'react';
import {Text as DefaultText, TextProps} from 'react-native';
import {styles} from './styles';

type Props = {
  children: string | JSX.Element | JSX.Element[];
} & TextProps;

export const Text: React.FC<Props> = ({children, ...rest}) => {
  return (
    <DefaultText {...rest} style={styles.text}>
      {children}
    </DefaultText>
  );
};
