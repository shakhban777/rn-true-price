import { StyleSheet } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

export const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    color: Colors.lighter
  },
});
