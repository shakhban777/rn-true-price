import { StyleSheet, useColorScheme } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

export const styles = StyleSheet.create({
  input: {
    height: 40,
    paddingHorizontal: 15,
    borderWidth: 1,
    borderRadius: 25,
    borderColor: Colors.lighter,
    color: Colors.lighter,
  },
});
