import React from 'react';
import {
  NativeSyntheticEvent,
  StyleProp,
  TextInput,
  TextInputChangeEventData,
  TextInputProps,
  TextStyle,
} from 'react-native';
import {styles} from './styles';

type Props = {
  value: string;
  onChange?: (e: NativeSyntheticEvent<TextInputChangeEventData>) => void;
  style?: StyleProp<TextStyle>;
} & TextInputProps;

export const Input = React.forwardRef<TextInput | null, Props>(
  ({value, onChange, style, ...rest}, ref) => {
    return (
      <TextInput
        style={[styles.input, style]}
        value={value}
        onChange={onChange}
        ref={ref}
        {...rest}
      />
    );
  },
);
