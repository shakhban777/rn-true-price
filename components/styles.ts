import {StyleSheet} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    elevation: 3,
    width: 50,
    height: 50,
  },
  backgroundStyle: {
    // backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    backgroundColor: Colors.darker,
    flex: 1,
    padding: 10,
  },
  main: {
    flexDirection: 'row',
    columnGap: 15,
    maxWidth: '100%',
    flex: 1,
  },
  lastChild: {
    marginBottom: 50,
  },
  footer: {
    position: 'absolute',
    bottom: 15,
    right: 15,
  },
  clearButton: {
    width: '25%',
  },
  row: {
    flexDirection: 'column',
    rowGap: 10,
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'flex-end',
  },
});
