export const convertPrice = (currentPrice: number): number =>
  Math.round(currentPrice * 100) / 100;

export function containsLetters(str: string): boolean {
  const regex = /[a-zA-Z]/;
  return regex.test(str);
}

export function numberHasTwoOrMoreDots(str: string): boolean {
  const indices: number[] = [];
  for (let i = 0; i < str.length; i++) {
    if (str[i] === '.' || str[i] === ',') {
      indices.push(i);
    }
  }
  return indices.length >= 2;
}
